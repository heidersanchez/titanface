from django import forms
from django.core.validators import FileExtensionValidator
import os

class UploadImageForm(forms.Form):
    # You can change this to any folder on your system
    ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif']
    file = forms.FileField(validators=[FileExtensionValidator(ALLOWED_EXTENSIONS)])