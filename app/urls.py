# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from app import views


urlpatterns = [

    # The home page
    path('', views.index, name='home'),
    path('faced/fromimage.html', views.faced_fromimage, name='faced_fromimage'),
    path('faced/fromwebcam.html', views.faced_fromwebcam, name='faced_fromwebcam'),
    path('faced/video_viewer.html', views.video_viewer, name='video_viewer'),

    # Matches any html file
    re_path(r'^.*\.html', views.pages, name='pages'),
]

