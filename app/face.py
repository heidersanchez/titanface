import os
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from PIL import Image,ImageDraw
import face_recognition
import random
import cv2
import threading
import numpy as np

def random_color():
    rgbl=[255,0,0]
    random.shuffle(rgbl)
    return tuple(rgbl)

def process_image(file):
    fs = FileSystemStorage()
    filename = fs.save(file.name, file)

    image = face_recognition.load_image_file(file)
    face_locations = face_recognition.face_locations(image)
    image = Image.fromarray(image)
    draw = ImageDraw.Draw(image)

    for face_location in face_locations:
        top, right, bottom, left = face_location
        draw.rectangle(((left, top), (right, bottom)), outline=random_color(), width=3)

    image.save(f"{settings.MEDIA_ROOT}/{filename}")
    
    return fs.url(filename)

###############################################################################################

video_camera = None
global_frame = None

class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)
        (self.grabbed, self.frame) = self.video.read()
        threading.Thread(target=self.update, args=()).start()

    def __del__(self):
        self.video.release()

    def get_frame(self):
        if self.grabbed:
            face_locations = face_recognition.face_locations(self.frame)

            image = Image.fromarray(self.frame)
            draw = ImageDraw.Draw(image)
            for face_location in face_locations:
                top, right, bottom, left = face_location
                draw.rectangle(((left, top), (right, bottom)), outline=random_color(), width=3)

            _, jpeg = cv2.imencode('.jpg', np.asarray(image))
            return jpeg.tobytes()
        else:
            return None

    def update(self):
        while True:
            (self.grabbed, self.frame) = self.video.read()

def video_stream():
    global video_camera
    global global_frame

    if video_camera == None:
        video_camera = VideoCamera()

    while True:
        frame = video_camera.get_frame()
        if frame != None:
            global_frame = frame
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        else:
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + global_frame + b'\r\n\r\n')