# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from .forms import UploadImageForm
from .face import *
from django.views.decorators import gzip
from django.http import StreamingHttpResponse


@login_required(login_url="/login/")
def index(request):
    context = {}
    context['segment'] = 'index'
    html_template = loader.get_template( 'index.html' )
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:
        
        load_template      = request.path.split('/')[-1]
        context['segment'] = load_template
        
        html_template = loader.get_template( load_template )
        return HttpResponse(html_template.render(context, request))
        
    except template.TemplateDoesNotExist:

        html_template = loader.get_template( 'page-404.html' )
        return HttpResponse(html_template.render(context, request))

    except:
    
        html_template = loader.get_template( 'page-500.html' )
        return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def faced_fromimage(request):
    context = {}
    context['segment'] = 'faced_fromimage'
    html_template = loader.get_template('app/faced_fromimage.html')

    if request.method == "POST":
        form = UploadImageForm(request.POST, request.FILES)
        file = request.FILES['file']
        if form.is_valid() and file:
            if 'detect' in request.POST:
                context['img_url'] = process_image(file)
            else:
                print("Reconocer")
    else:
        form = UploadImageForm()
    context['form'] = form
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def faced_fromwebcam(request):
    context = {}
    context['segment'] = 'faced_fromwebcam'
    html_template = loader.get_template('app/faced_fromwebcam.html')
    return HttpResponse(html_template.render(context, request))

@gzip.gzip_page
def video_viewer(request):
    try:
        return StreamingHttpResponse(video_stream(), content_type="multipart/x-mixed-replace;boundary=frame")
    except:
        pass
